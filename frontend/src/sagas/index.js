import UserActionHandlers from './user';
import PlanActionHandlers from './plan';

export default function* rootSaga() {
  yield [
    ...UserActionHandlers,
    ...PlanActionHandlers,
  ]
}
