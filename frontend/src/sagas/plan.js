import { put, takeLatest, call } from "redux-saga/effects";
import { get } from 'lodash';

import {reset} from 'redux-form';

import {
  planUploadRequest,
  planUploadReceive,
  planUploadFail,
  planProcessRequest,
  planProcessReceive,
  planProcessFail,
  planUpdateRequest,
  planUpdateReceive,
  planUpdateFail,
  plansGetRequest,
  plansGetReceive,
  plansGetFail,
} from "../actions/plan";

import {
  startLoading,
  finishLoading,
} from "../actions/data";

import {
  PLAN_UPLOAD,
  PLAN_PROCESS,
  PLANS_GET,
  PLAN_DOWNLOAD,
  PLAN_UPDATE,
} from "../constants/actions";

import * as Api from "../util/api";

function* planUpload(action) {
  try {
    yield put(planUploadRequest(action));
    yield put(startLoading());
    const { planId } = yield Api.planUpload(action.payload);
    yield put(planUploadReceive());
    yield put(finishLoading());
    yield call(planProcess, { payload: planId });
  } catch (err) {
    const message = get(err, 'response.data.message') || 'Ошибка загрузки документа!';
    yield put(planUploadFail(err));
    yield put(reset('newPlanForm'));
    yield put(finishLoading());
    alert(message);

  }
}

function* planProcess(planData) {
  try {
    yield put(planProcessRequest());
    yield put(startLoading());
    const planId = get(planData, 'payload');
    const plan = yield Api.planProcess(planId);
    yield put(planProcessReceive(plan));
    yield put(finishLoading());
    alert('Документ успешно обработан!');
  } catch (err) {
    const message = get(err, 'response.data.message') || 'Ошибка обработки документа!';
    yield put(planProcessFail(err));
    yield put(reset('newPlanForm'));
    yield put(finishLoading());
    alert(message);
  }
}

function* planUpdate(action) {
  try {
    yield put(planUpdateRequest());
    yield put(startLoading());
    const { planId, planData } = get(action, 'payload');
    const plan = yield Api.planUpdate(planId, planData);
    yield put(planUpdateReceive(plan));
    yield put(finishLoading());
    yield call(planProcess, { payload: planId });
  } catch (err) {
    const message = get(err, 'response.data.message') || 'Ошибка перезаписи плана!';
    yield put(planUpdateFail(err));
    yield put(finishLoading());
    alert(message);
  }
}

function* plansGet(action) {
  try {
    yield put(plansGetRequest());
    yield put(startLoading());
    const plans = yield Api.plansGet();
    yield put(plansGetReceive(plans));
    yield put(finishLoading());
  } catch (err) {
    const message = get(err, 'response.data.message') || 'Невозможно получить список планов!';
    yield put(plansGetFail(err));
    alert(message);
    yield put(finishLoading());
  }
}

function* planDownload(planData) {
  try {
    yield put(startLoading());
    const planId = get(planData, 'payload');
    Api.downloadPlan(planId);
    yield put(finishLoading());
  } catch (err) {
    const message = get(err, 'response.data.message') || 'Ошибка загрузки документа!';
    yield put(finishLoading());
    alert(message);
  }
}

export default [
  takeLatest(PLAN_UPLOAD.ACTION, planUpload),
  takeLatest(PLAN_PROCESS.ACTION, planProcess),
  takeLatest(PLANS_GET.ACTION, plansGet),
  takeLatest(PLAN_DOWNLOAD.ACTION, planDownload),
  takeLatest(PLAN_UPDATE.ACTION, planUpdate),
]
