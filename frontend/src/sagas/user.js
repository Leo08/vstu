import { delay } from "redux-saga";
import { put, takeLatest, race, fork, take, call } from "redux-saga/effects";
import { get, includes, toLower } from "lodash";
import { stopAsyncValidation } from "redux-form";
import { push } from "connected-react-router";
import {
  setAuthToken,
  getUserInfoFromToken,
  getTokenLifeTime
} from "../util/token";

import {
  authCheckRequest,
  authCheckReceive,
  authCheckFail,
  loginUserReceive,
  loginUserFail,
  loginUserRequest,
  logoutUserReceive,
  logoutUserFail,
  logoutUserRequest,
  signupUserRequest,
  signupUserFail
} from "../actions/user";

import {
  LOGOUT_USER,
  AUTH_CHECK,
  LOGIN_USER,
  SIGNUP_USER,
} from "../constants/actions";

import * as Api from "../util/api";

function getErrorsObject(message) {
  const errors = {};
  if (
    includes(toLower(message), "user") ||
    includes(toLower(message), "email")
  ) {
    errors.username = message;
  }
  if (includes(toLower(message), "password")) {
    errors.password = message;
  }
  return errors;
}

function* authFlowSaga() {
  let token, newToken, tokenLifeTime;
  try {
    while (true) {
      const { payload } = yield take(LOGIN_USER.ACTION);
      let result = yield Api.login(payload.email, payload.password);
      token = result.token;
      setAuthToken(token);
      const userInfo = getUserInfoFromToken(token);
      tokenLifeTime = getTokenLifeTime(token);
      yield put(loginUserReceive(userInfo));
      if (!token) continue;
      let userSignedOut;
      while (!userSignedOut) {
        const { expired } = yield race({
          expired: delay(tokenLifeTime),
          signout: take(LOGOUT_USER.ACTION)
        });
        try{
        if (expired) {
          const result = yield call(Api.updateToken);
          newToken = result.data.token;
          setAuthToken(newToken);
          tokenLifeTime = getTokenLifeTime(newToken);
          const userInfo = getUserInfoFromToken(token);
          yield put(authCheckReceive(userInfo))
          if (!newToken) {
            userSignedOut = true;
            yield put(logoutUserRequest(LOGOUT_USER.ACTION));
            sessionStorage.clear();
            yield put(logoutUserReceive());
          }
        } else {
          userSignedOut = true;
          yield put(logoutUserRequest(LOGOUT_USER.ACTION));
          sessionStorage.clear();
          yield put(logoutUserReceive());
        }
      } catch(err){
          userSignedOut = true;
          yield put(logoutUserRequest(LOGOUT_USER.ACTION));
          sessionStorage.clear();
          yield put(logoutUserReceive());
      }
      }
    }
  } catch (err) {
    console.log(err);
  }
}

function* checkAuthenticated(action) {
  try {
    yield put(authCheckRequest(action));
    const authenticated = yield Api.checkAuthenticated();
    yield put(authCheckReceive(authenticated));
  } catch (err) {
    yield put(authCheckFail(err));
  }
}

function* signupUser(action) {
  try {
    yield put(signupUserRequest(action));
    const { firstName, lastName, email, password, phone } = get(
      action,
      "payload"
    );
    yield Api.signup(firstName, lastName, email, phone, password);
    yield put(push("/signin"));
  } catch (err) {
    yield put(signupUserFail(err));
  }
}

export default [
  takeLatest(AUTH_CHECK.ACTION, checkAuthenticated),
  fork(authFlowSaga),
  takeLatest(SIGNUP_USER.ACTION, signupUser),

]
