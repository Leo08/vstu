import { defineAction } from 'redux-define';

export const DEFAULT_API_ACTION_STATES = ['REQUEST', 'RECEIVE', 'FAIL'];

export const AUTH_CHECK = defineAction('AUTH_CHECK', DEFAULT_API_ACTION_STATES);
export const LOGIN_USER = defineAction('LOGIN_USER', DEFAULT_API_ACTION_STATES);
export const LOGOUT_USER = defineAction('LOGOUT_USER', DEFAULT_API_ACTION_STATES);
export const SIGNUP_USER = defineAction('SIGNUP_USER',DEFAULT_API_ACTION_STATES);

export const PLAN_UPLOAD = defineAction('PLAN_UPLOAD', DEFAULT_API_ACTION_STATES);
export const PLAN_PROCESS = defineAction('PLAN_PROCESS', DEFAULT_API_ACTION_STATES);
export const PLANS_GET = defineAction('PLANS_GET', DEFAULT_API_ACTION_STATES);
export const PLAN_DOWNLOAD = defineAction('PLAN_DOWNLOAD', DEFAULT_API_ACTION_STATES);
export const PLAN_UPDATE = defineAction('PLAN_UPDATE', DEFAULT_API_ACTION_STATES);

export const START_LOADING = defineAction('START_LOADING');
export const FINISH_LOADING = defineAction('FINISH_LOADING');
