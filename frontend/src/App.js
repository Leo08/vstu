import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { initStore, getStore } from './store';
import AppRouter from './containers/AppRouterContainer';

class App extends Component {
  componentWillMount() {
    initStore();
  }
  render() {
    return (
      <Provider store={getStore()}>
        <AppRouter />
      </Provider>
    );
  }
}

export default App;
