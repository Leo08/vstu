import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createBrowserHistory } from 'history';
import createSagaMiddleware from 'redux-saga';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import logger from 'redux-logger';

import sagas from './sagas';
import reducers from './reducers';

let store = null;
let history = null;

export function initStore() {
  const isDev = process.env.NODE_ENV !== 'production';

  history = createBrowserHistory();
  const sagaMiddleware = createSagaMiddleware();
  const middlewares = [ routerMiddleware(history), sagaMiddleware ];
  if (isDev) {
    middlewares.push(logger);
  }
  const middleware = applyMiddleware.apply(null, middlewares);
  store = createStore(
    connectRouter(history)(reducers),
    isDev
      ? composeWithDevTools(middleware)
      : middleware
  );

  sagaMiddleware.run(sagas);
  return store;
}

export function getStore() {
  return store;
}

export function getHistory() {
  return history;
}

export function dispatch(action) {
  return store.dispatch(action);
}

export function getState() {
  return store.getState();
}
