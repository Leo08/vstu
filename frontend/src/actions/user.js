import { createAction } from "redux-actions";
import {
  AUTH_CHECK,
  LOGIN_USER,
  LOGOUT_USER,
  SIGNUP_USER,
} from "../constants/actions";

export const authCheck = createAction(AUTH_CHECK.ACTION);
export const authCheckRequest = createAction(AUTH_CHECK.REQUEST);
export const authCheckReceive = createAction(AUTH_CHECK.RECEIVE);
export const authCheckFail = createAction(AUTH_CHECK.FAIL);

export const loginUser = createAction(LOGIN_USER.ACTION);
export const loginUserRequest = createAction(LOGIN_USER.REQUEST);
export const loginUserReceive = createAction(LOGIN_USER.RECEIVE);
export const loginUserFail = createAction(LOGIN_USER.FAIL);

export const logoutUser = createAction(LOGOUT_USER.ACTION);
export const logoutUserRequest = createAction(LOGOUT_USER.REQUEST);
export const logoutUserReceive = createAction(LOGOUT_USER.RECEIVE);
export const logoutUserFail = createAction(LOGOUT_USER.FAIL);

export const signupUser = createAction(SIGNUP_USER.ACTION);
export const signupUserRequest = createAction(SIGNUP_USER.REQUEST);
export const signupUserReceive = createAction(SIGNUP_USER.RECEIVE);
export const signupUserFail = createAction(SIGNUP_USER.FAIL);
