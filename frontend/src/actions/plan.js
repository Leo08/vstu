import { createAction } from "redux-actions";
import {
  PLAN_UPLOAD,
  PLAN_PROCESS,
  PLANS_GET,
  PLAN_DOWNLOAD,
  PLAN_UPDATE,
} from "../constants/actions";

export const planUpload = createAction(PLAN_UPLOAD.ACTION);
export const planUploadRequest = createAction(PLAN_UPLOAD.REQUEST);
export const planUploadReceive = createAction(PLAN_UPLOAD.RECEIVE);
export const planUploadFail = createAction(PLAN_UPLOAD.FAIL);

export const planProcess = createAction(PLAN_PROCESS.ACTION);
export const planProcessRequest = createAction(PLAN_PROCESS.REQUEST);
export const planProcessReceive = createAction(PLAN_PROCESS.RECEIVE);
export const planProcessFail = createAction(PLAN_PROCESS.FAIL);

export const plansGet = createAction(PLANS_GET.ACTION);
export const plansGetRequest = createAction(PLANS_GET.REQUEST);
export const plansGetReceive = createAction(PLANS_GET.RECEIVE);
export const plansGetFail = createAction(PLANS_GET.FAIL);

export const planDownload = createAction(PLAN_DOWNLOAD.ACTION);
export const planDownloadRequest = createAction(PLAN_DOWNLOAD.REQUEST);
export const planDownloadReceive = createAction(PLAN_DOWNLOAD.RECEIVE);
export const planDownloadFail = createAction(PLAN_DOWNLOAD.FAIL);

export const planUpdate = createAction(PLAN_UPDATE.ACTION);
export const planUpdateRequest = createAction(PLAN_UPDATE.REQUEST);
export const planUpdateReceive = createAction(PLAN_UPDATE.RECEIVE);
export const planUpdateFail = createAction(PLAN_UPDATE.FAIL);
