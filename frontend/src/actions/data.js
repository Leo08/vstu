import { createAction } from "redux-actions";
import {
  START_LOADING,
  FINISH_LOADING,
} from "../constants/actions";

export const startLoading = createAction(START_LOADING);
export const finishLoading = createAction(FINISH_LOADING);
