import Home from './components/pages/Home';
import NewPlan from './containers/NewPlanContainer';
import PlansList from './containers/PlansListContainer';

export function getRoutes() {
  return [
    {
      path: '/new-plan',
      title: 'Новый план',
      className: '',
      component: NewPlan,
      inMainMenu: true,
      isPrivate: false,
    },
    {
      path: '/plans-list',
      title: 'Список планов',
      className: '',
      component: PlansList,
      inMainMenu: true,
      isPrivate: false,
    },
    {
      path: '/signin',
      title: 'Sign in',
      className: '',
      component: Home,
      inAuthMenu: true,
      isPrivate: false,
    }
  ]
}
