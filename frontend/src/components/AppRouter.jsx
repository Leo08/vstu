import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { Route, Switch, Redirect, matchPath } from 'react-router';
import { ConnectedRouter } from 'connected-react-router';

import { getHistory } from '../store';

import Home from './pages/Home';
import NoMatch from './pages/NoMatch';
import { getRoutes } from '../routes';

class AppRouter extends PureComponent {
  constructor(props) {
    super(props);
    this.routes = getRoutes();
    this.privateRoutes = this.routes.filter(route => route.isPrivate);
  }

  componentWillMount() {
    //this.props.authCheck();
  }

  isPrivateRoute() {
    const { currentLocationPath } = this.props;
    let match = false;
    this.privateRoutes.forEach((route) => {
      if (!!matchPath(currentLocationPath, { path: route.path }))
        match = true;
    });
    return match;

  }

  redirectToLogin() {
    const { isUserAuthenticated, location } = this.props;
    if (isUserAuthenticated) {
      return null;
    }
    if (this.isPrivateRoute()) {
      return (
        <Redirect to={{
          pathname: "/signin",
          state: { from: location },
        }} />
      )
    }
    return null;
  }

  render() {
    return (
      <ConnectedRouter history={getHistory()}>
        <div>
          <Switch>
            <Route exact path="/" component={Home} />
            {
              this.routes.map((route) => {
                return (
                  <Route
                    key={route.path}
                    path={route.path}
                    render={(props) => {
                      window.scrollTo(0, 0);
                      return (
                        <route.component {...props} />
                      )
                    }}
                  />
                )
              })
            }
            <Route path="*" component={NoMatch} />
          </Switch>
          {this.redirectToLogin()}
        </div>
      </ConnectedRouter>
    )
  }
}

AppRouter.propTypes = {
  authCheck: PropTypes.func.isRequired,
  isUserAuthenticated: PropTypes.bool,
  currentLocationPath: PropTypes.string,
}

AppRouter.defaultProps = {
  currentLocationPath: '',
}

export default AppRouter;
