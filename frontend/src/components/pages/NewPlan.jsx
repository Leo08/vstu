import React, { PureComponent } from 'react';
import { reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import { Row, Col, Button } from 'react-bootstrap';

import PageLayout from '../../containers/PageLayoutContainer';
import FileInput from '../FileInput';

class NewPlan extends PureComponent {
  render() {
    const { planUpload, handleSubmit, pristine } = this.props;
    return (
      <PageLayout
        className="plan-upload-page"
        title="Добавление плана"
      >
      <form onSubmit={handleSubmit(planUpload)} className="central-form text-center">
        <FileInput
         name="plan"
         label="Загрузка документа"
          classNameLabel="file-input-label"
          className="file-input"
          dropzone_options={{
            multiple: false,
            accept: '.docx'
          }}
          required
        />
        <Row>
          <Col className="text-center">
            <Button variant="outline-primary" type="submit" disabled={pristine}>Загрузить</Button>
          </Col>
        </Row>
      </form>
      </PageLayout>
    )
  }
}

NewPlan.propTypes = {
  planUpload: PropTypes.func.isRequired,
}

export default reduxForm({
  form: 'newPlanForm',
})(NewPlan);
