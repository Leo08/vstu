import React, { PureComponent } from 'react';

import PlansList from '../../containers/PlansListContainer';

class Home extends PureComponent {
  render() {
    return <PlansList />
  }
}

export default Home;
