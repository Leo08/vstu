import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { orderBy } from 'lodash';
import { Pagination, Dropdown, Button, Table, ButtonToolbar, Row, Col } from 'react-bootstrap';

import PageLayout from '../../containers/PageLayoutContainer';
import './PlansList.css';

const columns = {
  id: "#",
  registration_number: "Регистрационный номер",
  group_name: "Группа",
  year: "Год",
  shifr: "Специальность",
  speciality_form: "Форма обучения",
  date_approve: "Утверждено",
}

class PlansList extends React.Component {
  state = {
    order: 'desc',
    orderField: 'id',
    page: 0,
    rowsPerPage: 5,
    filters:{}
  };

  componentWillMount() {
    this.props.plansGet();
  }

  filterPlans() {
    const { filters } = this.state;
    const filterKeys = Object.keys(filters);

    return this.props.plans.filter(plan => {
      let filtersRes = true;
      filterKeys.forEach(filter => {
        const val = filters[filter].toLowerCase().trim();
        if (val.length > 0) {
          if (plan[filter].toString().toLowerCase().indexOf(val) === -1) {
            filtersRes = false;
          }
        }
      });
      return filtersRes;
    });
  }

  getPlans() {
    const { order, orderField, page, rowsPerPage } = this.state;
    const orderedPlans = orderBy(this.filterPlans(), [orderField], [order]);
    const sliceBegin = (page * rowsPerPage > orderedPlans.length) ? 0 : page * rowsPerPage;
    return orderedPlans.slice(sliceBegin, sliceBegin + rowsPerPage)
  }

  getOrderElement(column) {
    const { order, orderField } = this.state;
    if (column === orderField) {
      return order === 'desc' ? (<i className="fas fa-sort-amount-up"></i>) : (<i className="fas fa-sort-amount-down"></i>);
    }
    return null;
  }

  changeSorting(column) {
    const { order, orderField } = this.state;
    const newOrder = orderField === column
      ? order === 'desc' ? 'asc' : 'desc'
      : 'asc';
    this.setState({
      order: newOrder,
      orderField: column,
    });
  }

  getHeaderColumns() {
    return Object.keys(columns).map(column => {
      return (
        <th key={ column } onClick={ () => this.changeSorting(column) }>{ columns[column] }{ this.getOrderElement(column) }</th>
      )
    })
  }

  setFilter(column, val) {
    this.setState({
      filters: {
        ...this.state.filters,
        [column]: val,
      }
    })
  }

  getHeaderColumnsSearch() {
    return Object.keys(columns).map(column => (
        <th key={ column }><input type="text" onChange={(e) => { this.setFilter(column, e.target.value) }} value={ this.state.filters[column] }/></th>
    ))
  }

  changePage(newPage) {
    const { rowsPerPage } = this.state;
    const { plans } = this.props;
    const page = ((newPage < 0) || (newPage * rowsPerPage > plans.length)) ? 0 : newPage;
    this.setState({
      page,
    })
  }

  getPagination() {
    const { rowsPerPage, page } = this.state;
    const { plans } = this.props;
    const pagesCount = Math.ceil(plans.length / rowsPerPage);
    const pages = [];
    for (let i = 0; i < pagesCount; i++ ) {
      pages.push(i+1);
    }
    return (
      <Pagination>
        { pages.map((pageEl, index) => {
          return (
            <Pagination.Item key={index} active={page === index} onClick={ () => this.changePage(index) }>
              {pageEl}
            </Pagination.Item>
          )
        })}
      </Pagination>
    )
  }

  planUpdate(planId) {
    let input = document.createElement("input");
    input.type = "file";
    input.accept = ".docx";
    input.onchange = (fileInput) => {
      const f = fileInput.target.files;
      const data = new FormData();
      data.append('file', f[0]);
      this.props.planUpdate(planId, data);
    };
    input.click();
  }

  render() {
    const { rowsPerPage } = this.state;
    const plans = this.getPlans();
    return (
      <PageLayout
        className="plans-list-page"
        title="Список планов"
      >
        <Table striped bordered hover className="plans-list">
          <thead>
	    <tr>
              { this.getHeaderColumns() }
              <th rowspan="2" className="last-header-cell">Действия</th>
            </tr>
            <tr>
              { this.getHeaderColumnsSearch() }
            </tr>
            
          </thead>
          <tbody>
            {!!plans.length && plans.map(plan => (
              <tr key={ plan.id }>
                { Object.keys(columns).map(column => (
                  <td key={column}>{ plan[column] ? plan[column] : (<span className="no-data">Нет данных</span>)}</td>
                ))}
                <td>
                  <ButtonToolbar>
                    { !plan.processed && (
                      <Button variant="outline-secondary" onClick={ () => this.props.planProcess(plan.id) } title="Обработать"><i className="fas fa-database"></i></Button>
                    ) }
		    &nbsp;
                    { plan.file_name && (
                      <Button variant="outline-primary" onClick={ () => this.props.planDownload(plan.id) } title="Скачать"><i className="fas fa-file-download"></i></Button>
                    ) }
		    &nbsp;
                    <Button variant="outline-success" onClick={ () => this.planUpdate(plan.id) }title="Перезапись"><i className="fas fa-cloud-upload-alt"></i></Button>
                  </ButtonToolbar>
                </td>
              </tr>
            ))}
            {!plans.length && (
	        <tr>
		    <td colspan="8" style={{ textAlign: "center" }}>
			<h4>Нет планов</h4>
		    </td>
		</tr>
            )}
          </tbody>
        </Table>

        <Row>
          <Col>{ this.getPagination() }</Col>
          <Col className="text-right">
            <Dropdown className="pull-right">
            Планов на странице &nbsp;
            <Dropdown.Toggle variant="primary">
              { rowsPerPage }
            </Dropdown.Toggle>

            <Dropdown.Menu>
              <Dropdown.Item as="button" onSelect={e => this.setState({ rowsPerPage: 5 })}>5</Dropdown.Item>
              <Dropdown.Item as="button" onSelect={e => this.setState({ rowsPerPage: 10 })}>10</Dropdown.Item>
              <Dropdown.Item as="button" onSelect={e => this.setState({ rowsPerPage: 15 })}>15</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
          </Col>
        </Row>
        <Row>
          <Col className="text-center">
            <Link to={ `/new-plan` }>
              <Button variant="outline-primary">Добавить план</Button>
            </Link>          
          </Col>
        </Row>

      </PageLayout>
    );
  }
}

PlansList.propTypes = {
  plans: PropTypes.array,
  plansGet: PropTypes.func.isRequired,
  //plansDelete: PropTypes.func.isRequired,
  planProcess: PropTypes.func.isRequired,
  planDownload: PropTypes.func.isRequired,
  planUpdate: PropTypes.func.isRequired,
};

export default PlansList;
