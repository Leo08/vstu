import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import PageLayout from '../../containers/PageLayoutContainer';

class NoMatch extends PureComponent {
  render() {
    return (
      <PageLayout title="Страница не найдена">
        <h1>Страница не найдена</h1><br />
        Вы можете вернуться на <Link to="/" className="link">главную.</Link>
      </PageLayout>
    )
  }
}

export default NoMatch;
