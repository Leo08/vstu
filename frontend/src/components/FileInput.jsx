import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';

class FileInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fileName: null,
      fileSize: 0,
    }
    this.selectFile = this.selectFile.bind(this);
  }
  selectFile(fileName, fileSize) {
    this.setState({
      fileName,
      fileSize,
    });
  }
  render() {
    const { fileName, fileSize } = this.state;
    const { className, input: { onChange }, dropzone_options, meta: { error, touched }, label, classNameLabel, name, cbFunction } = this.props;

    return (
      <div className={`${className}` + (error && touched ? ' has-error ' : '')}>
        {label && <p className={classNameLabel || ''}>{label}</p>}
        <Dropzone
          {...dropzone_options}
          onDrop={(f) => {
            if (f.length < 1) {
              return false;
            }
            const data = new FormData();
            data.append('file', f[0]);
            this.selectFile(f[0].name, f[0].size);
            cbFunction(data);
            return onChange(data);
          }}
          className="dropzone-input"
          name={name}
        >
          {({getRootProps, getInputProps, isDragActive}) => {
            return (
              <div
              {...getRootProps()}
              className={['dropzone', isDragActive ? 'dropzone--isActive' : ''].join(' ')}
            >

              <input {...getInputProps()} />
              { fileName ?
                <p><strong>Выбран: </strong>{ fileName } ({ (fileSize/1000).toFixed(2) } Кб)</p>:
                isDragActive ?
                  <p>Перетяните файл сюда...</p> :
                  <p>Перетяните файл или нажмите для выбора файла...</p>
              }
            </div>
            )
          }}
        </Dropzone>
        {error && touched ? error : ''}
      </div>
    );
  }
}

FileInput.propTypes = {
  dropzone_options: PropTypes.object,
  meta: PropTypes.object,
  label: PropTypes.string,
  classNameLabel: PropTypes.string,
  input: PropTypes.object,
  className: PropTypes.string,
  cbFunction: PropTypes.func,
}

FileInput.defaultProps = {
  className: '',
  cbFunction: () => {},
}

export default props => <Field {...props} component={FileInput} />;
