import React, { Component } from "react";
import PropTypes from 'prop-types';
import { Container, Row, Col } from 'react-bootstrap';

import NavigationMenu from "../NavigationMenu/NavigationMenu";
import { getRoutes } from "../../routes";
import Loader from '../Loader';


class PageLayout extends Component {
  constructor(props) {
    super(props);
    const routes = getRoutes();
    this.routes = routes.filter(route => route.inMainMenu);
  }

  render() {
    const {
      children,
      isUserAuthenticated,
      currentUser,
      isLoading,
      title,
    } = this.props;

    if (isLoading) {
      return (
        <Loader />
      )
    }

    return (
      <div>
        <NavigationMenu
          navigationItems={this.routes}
          className="top-navigation-menu"
          title={title}
        />
        <Container>
          <Row>
            <Col>
              { children }
            </Col>
          </Row>
        </Container>
      </div>
    );

  }

}

PageLayout.propTypes = {
  children: PropTypes.node,
  isUserAuthenticated: PropTypes.bool.isRequired,
  currentUser: PropTypes.object,
  isLoading: PropTypes.bool.isRequired,
  title: PropTypes.string,
};

PageLayout.defaultProps = {
  isUserAuthenticated: false,
}

export default PageLayout;
