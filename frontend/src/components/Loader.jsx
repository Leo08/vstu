import React, { PureComponent } from 'react';
import './Loader.css';

class Loader extends PureComponent {
  render() {
    return (
      <div id="page-loader">
        <div className="loader-ring"></div>
      </div>
    );
  }
}

export default Loader;
