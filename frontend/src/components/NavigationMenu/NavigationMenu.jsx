import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { Navbar, Nav } from 'react-bootstrap';


class NavigationMenu extends Component {

  getNavigationItems() {
    const { navigationItems } = this.props;

    if (navigationItems.length === 0) {
      return null;
    }

    const navItems = navigationItems.map((item, key) => {
      return (
        <li key={key} className={ item.className || '' }>
          <NavLink to={ item.path } activeClassName="active" className="nav-link">{ item.title }</NavLink>
        </li>
      )
    });

    return navItems;
  }
  render() {
    return (
      <Navbar bg="light" expand="lg">
        <Navbar.Brand href="/">ВГТУ планы</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            { this.getNavigationItems() }
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

NavigationMenu.propTypes = {
  navigationItems: PropTypes.array,
}

NavigationMenu.defaultProps = {
  navigationItems: [],
}

export default NavigationMenu;
