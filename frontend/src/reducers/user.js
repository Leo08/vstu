import { handleActions } from 'redux-actions';

import {
  authCheckReceive,
  authCheckFail,
  loginUserReceive,
  loginUserFail,
  logoutUserReceive,
} from '../actions/user';
import { omit } from 'lodash';

const initialState = {
  isUserAuthenticated: !!sessionStorage.getItem('jwToken'),
};

export const reducer = handleActions(
  {
    [authCheckReceive](state, { payload }) {
      return {
        ...state,
        isUserAuthenticated: true,
        currentUser: payload,
      }
    },
    [authCheckFail](state, { payload }) {
      return {
        ...state,
        isUserAuthenticated: false,
        currentUser: null,
      }
    },
    [loginUserReceive](state, { payload }) {
      return {
        ...state,
        isUserAuthenticated: true,
        currentUser: omit(payload, ['id']),
      }
    },
    [loginUserFail](state, { payload }) {
      return {
        ...state,
        isUserAuthenticated: false,
        currentUser: null,
      }
    },
    [logoutUserReceive](state, { payload }) {
      return {
        ...state,
        isUserAuthenticated: false,
        currentUser: null,
      }
    },
  },
  initialState
)
