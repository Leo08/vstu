import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';

import { reducer as user } from './user';
import { reducer as plan } from './plan';
import { reducer as data } from './data';

const reducer = combineReducers({
  router: routerReducer,
  form: formReducer,
  user,
  plan,
  data,
})

export default reducer;
