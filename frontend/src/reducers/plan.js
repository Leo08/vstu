import { handleActions } from 'redux-actions';

import {
  plansGetReceive,
  plansGetFail,
  planProcessReceive,
} from '../actions/plan';

const initialState = {
  list: [],
};

export const reducer = handleActions(
  {
    [plansGetReceive](state, { payload }) {
      return {
        ...state,
        list: payload,
      }
    },
    [plansGetFail](state, { payload }) {
      return {
        ...state,
        list: [],
      }
    },
    [planProcessReceive](state, { payload }) {
      return {
        ...state,
        list: [
          ...state.list.filter(el => el.id !== payload.id),
          payload,
        ]
      }
    },
  },
  initialState
)
