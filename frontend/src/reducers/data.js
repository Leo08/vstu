import { handleActions } from 'redux-actions';

import {
  startLoading,
  finishLoading,
} from '../actions/data';

const initialState = {
  isLoading: false,
};

export const reducer = handleActions(
  {
    [startLoading](state, { payload }) {
      return {
        ...state,
        isLoading: true,
      }
    },
    [finishLoading](state, { payload }) {
      return {
        ...state,
        isLoading: false,
      }
    },
  },
  initialState
)
