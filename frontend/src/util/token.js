import jwt_decode from "jwt-decode";
import { addAuthTokenToHeader } from "./api";

const tokenDelay = process.env.TOKEN_DELAY;

export const setAuthToken = token => {
  sessionStorage.setItem("jwToken", token);
  addAuthTokenToHeader(token);
};

export const getTokenLifeTime = token => {
  const decoded = jwt_decode(token);
  let currentTime = Date.now();
  const tokenLifeTime = decoded.exp * 1000  - currentTime - tokenDelay;
  return tokenLifeTime;
};

export const getUserInfoFromToken = token => {
  const decoded = jwt_decode(token);
  let userInfo = decoded;
  delete userInfo.exp;
  delete userInfo.iat;
  return userInfo;
};
