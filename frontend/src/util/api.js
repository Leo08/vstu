import _axios from 'axios';
import { getStore } from '../store';
import { logoutUser } from '../actions/user';
const apiUrl = 'http://localhost:8000';

const axios = _axios.create({
  baseURL: apiUrl,
  timeout: 200000,
});

axios.interceptors.response.use(function (response) {
  return response;
}, function (error) {
  if (401 === error.response.status) {
    const store = getStore();
    store.dispatch(logoutUser());
  }
  return Promise.reject(error);
});

export const addAuthTokenToHeader = token => {
  if (token) {
      axios.defaults.headers.common['Authorization'] = token;
  }
  else {
      delete axios.defaults.headers.common['Authorization'];
  }
};

const getAuthorizationHeader = () => {
  const token = sessionStorage.getItem('jwToken') || null;
  return { Authorization: token ? token : '' }
}

export async function checkAuthenticated() {
  const authCheckResult = await axios.get('/user/auth', {
    headers: getAuthorizationHeader(),
  });
  return true
}

export async function planUpload(data) {
  const planUploadResult = await axios.post('/plan/upload', data);
  return planUploadResult.data
}

export async function planProcess(planId) {
  const planProcessResult = await axios.post('/plan/process', { planId });
  return planProcessResult.data;
}

export async function planUpdate(planId, data) {
  const planUpdateResult = await axios.post(`/plan/update/${planId}`, data);
  return planUpdateResult.data;
}

export async function plansGet() {
  const plansLoadResult = await axios.get('/plan/list');
  return plansLoadResult.data
}

export async function login(email, password) {
  const loginResult = await axios.post('/api/auth/login', { email, password });
  return loginResult.data;
}

export async function signup(firstName, lastName, email, phone, password){
  await axios.post('/api/auth/signup', { firstName, lastName, email, phone, password });
  return true;
}

export async function logout(data) {
  await axios.post('/user/logout');

  return true;
}

export function downloadPlan(planId) {
  const win = window.open(`${apiUrl}/plan/download/${planId}`, '_blank');
  win.focus();
}

export function updateToken() {
    return axios.post('/api/auth/token');
}

export function downloadPlanLink(id) {
  return `${apiUrl}/plan/download?id=${id}`;
}
