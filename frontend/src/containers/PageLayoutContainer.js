import { connect } from "react-redux";
import { get } from "lodash";

import PageLayout from "../components/PageLayout/PageLayout";

function mapStateToProps(state) {
  return {
    currentLocationPath: get(state, 'router.location.pathname'),
    currentUser: get(state, 'users.currentUser'),
    isUserAuthenticated: get(state, 'users.isUserAuthenticated'),
    isLoading: get(state, 'data.isLoading'),
  }
}

function mapDispatchToProps(dispatch) {
  return {

  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PageLayout);
