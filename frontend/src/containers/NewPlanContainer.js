import { connect } from "react-redux";

import NewPlan from "../components/pages/NewPlan";
import { planUpload } from '../actions/plan';

function mapStateToProps(state) {
  return {
  }
}

function mapDispatchToProps(dispatch) {
  return {
    planUpload: ({ plan }) => {
      dispatch(
        planUpload(plan)
      );
    },
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewPlan);
