import Loader from '../components/Loader';
import { connect } from 'react-redux';
import { get } from 'lodash';
import { authCheck } from '../actions/user';

export default connect(
  state => ({
    progress: get(state, 'data.progress'),
    isLoading: get(state, 'data.isLoading'),
  }),
  {
  }
)(Loader);
