import { connect } from "react-redux";

import PlansList from "../components/pages/PlansList";
import { plansGet, planProcess, planDownload, planUpdate } from '../actions/plan';
import { get } from "lodash";

function mapStateToProps(state) {
  return {
    plans: get(state, 'plan.list') || [],
  }
}

function mapDispatchToProps(dispatch) {
  return {
    plansGet: () => {
      dispatch(
        plansGet()
      );
    },
    planProcess: ( planId ) => {
      dispatch(
        planProcess(planId)
      );
    },
    planUpdate: ( planId, planData ) => {
      dispatch(
        planUpdate({planId, planData})
      );
    },
    planDownload: ( planId ) => {
      dispatch(
        planDownload(planId)
      );
    },
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PlansList);
