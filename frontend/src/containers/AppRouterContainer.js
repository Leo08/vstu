import AppRouter from '../components/AppRouter';
import { connect } from 'react-redux';
import { get } from 'lodash';
import { authCheck } from '../actions/user';

export default connect(
  state => ({
    isUserAuthenticated: get(state, 'users.isUserAuthenticated'),
    currentLocationPath: get(state, 'router.location.pathname'),
  }),
  {
    authCheck,
  }
)(AppRouter);
