-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 04, 2019 at 11:28 AM
-- Server version: 5.7.26-0ubuntu0.19.04.1
-- PHP Version: 7.2.17-0ubuntu0.19.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vstu`
--

-- --------------------------------------------------------

--
-- Table structure for table `certification`
--

CREATE TABLE `certification` (
  `id` int(10) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `id_plan` int(10) DEFAULT NULL,
  `ze` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `certification`
--

INSERT INTO `certification` (`id`, `name`, `id_plan`, `ze`) VALUES
(1, 'Государственный экзамен по направлению, специализации.', 1, 5.5);

-- --------------------------------------------------------

--
-- Table structure for table `competence`
--

CREATE TABLE `competence` (
  `id` int(10) NOT NULL,
  `code` varchar(10) DEFAULT NULL,
  `name_competence` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `competence`
--

INSERT INTO `competence` (`id`, `code`, `name_competence`) VALUES
(1, 'БПК-3', 'Быть способным использовать основные законы электротехники и владеть методами их применения, применять электронные элементы и приборы в системах автоматизации'),
(2, 'УК-1', 'Знать закономерности исторического развития и формирования государственных и общественных институтов белорусского этноса, особенности развития науки, техники и технологий в разные исторические периоды, быть способным анализировать процессы государственного строительства и определять социально-политическое значение исторических событий, личностей, артефактов и символов'),
(3, 'УК-2', 'Владеть высоким уровнем культуры политического мышления и поведения, понимать сущность, ценности и принципы идеологии белорусского государства, анализировать социально-политические процессы в стране и мире'),
(4, 'УК-3', 'Владеть культурой мышления, быть способным к восприятию, обобщению и анализу информации, философских, мировоззренческих и психолого-педагогических проблем в сфере межличностных отношений и профессиональной деятельности '),
(5, 'УК-4', 'Уметь анализировать и оценивать социально-значимые явления, события и процессы, использовать социологическую и экономическую информацию при решении аналитических, научных и профессиональных задач, быть способным к проявлению предпринимательской инициативы '),
(6, 'УК-5', 'Обладать базовыми навыками коммуникации в устной и письменной формах на государственном и иностранном языках для решения задач межличностного и межкультурного взаимодействия и производственных задач'),
(7, 'УК-6', 'Владеть навыками здоровьесбережения'),
(8, 'УК-7', 'Владеть основами психологии труда для решения задач профессиональной деятельности'),
(9, 'УК-8', 'Быть способным использовать основы правовых знаний в различных сферах жизнедеятельности, владеть навыками поиска нормативных правовых актов, анализа их содержания и применения для решения профессиональных задач'),
(10, 'БПК-1', 'Владеть основными понятиями и методами линейной алгебры, аналитической и векторной геометрии, математического анализа, дифференциального и интегрального исчислений, анализа функций одной и нескольких переменных,  применять полученные знания для решения прикладных, инженерных задач'),
(11, 'БПК-2', 'Владеть основными понятиями и законами физики, принципами экспериментального и теоретического изучения физических явлений и процессов, быть способным создавать и анализировать на основе физических законов теоретические модели, владеть навыками практического использования принципов и приемов физических измерений'),
(12, 'БПК-3', 'Владеть теоретическими положениями химии, техникой химических расчетов и методами химических экспериментальных исследований, быть способным прогнозировать свойства соединений на основании строения вещества, характера химического и межмолекулярного взаимодействия '),
(13, 'БПК-4', 'Владеть методами защиты производственного персонала и населения от негативных воздействий факторов антропогенного, техногенного, естественного происхождения, знаниями основ рационального природопользования и энергосбережения  '),
(14, 'БПК-5', 'Быть способным применять в профессиональной деятельности правовые, организационные и инженерные основы обеспечения безопасных и здоровых условий труда, производить оценку условий труда, выявлять опасные и вредные производственные факторы, принимать решения по нормализации условий труда'),
(15, 'БПК-6', 'Владеть методами графического изображения предметов на плоскости и в пространстве, навыками использования современного программного обеспечения для работы с графической информацией'),
(16, 'БПК-7', 'Владеть базовыми навыками расчетов и испытаний электрических схем электрооборудования'),
(17, 'БПК-8', 'Владеть инженерными методиками расчета, проектирования электронных устройств различного назначения, а также применения основных типов современных аналоговых и цифровых интегральных микросхем для решения инженерных задач'),
(18, 'БПК-9', 'Быть способным производить расчеты и разрабатывать электронные средства автоматизации'),
(19, 'БПК-10', 'Быть способным внедрять современные микропроцессорные системы автоматизации, осуществлять переналадку оборудования'),
(20, 'БПК-11', 'Быть способным использовать основы экономических знаний в сфере лёгкой промышленности и других областях'),
(21, 'БПК-12', 'Быть способным применять прогрессивные энергоэффективные и ресурсосберегающие технологии монтажа и наладки средств автоматизации'),
(22, 'СК-1', 'Быть способным формировать стратегию управления производством, осуществлять  организационно-технические расчеты для планирования  основного и вспомогательного производств, выполнять оценку эффективности мероприятий по техническому и организационному развитию производства '),
(23, 'СК-2', 'Владеть основами организации, структуры и функционирования автоматизированных систем управления производством'),
(24, 'СК-3', 'Быть способным применять физико-математические методы для расчетов механизмов, машин и конструкций, разрабатывать и анализировать их кинематические и динамические схемы'),
(25, 'СК-4', 'Владеть основами состава, строения, свойств и обработки материалов'),
(26, 'СК-5', 'Быть способным производить расчеты технических конструкций и их элементов на прочность, устойчивость, жесткость, знать устройство и принципы взаимодействия деталей машин общего назначения, определять рациональные варианты передач приводов машин и механизмов'),
(27, 'СК-6', 'Быть способным выполнять расчет систем автоматизированного гидропневмопривода для использования в системах автоматизации'),
(28, 'СК-7', 'Быть способным выполнять расчет систем автоматизированного электропривода для использования в системах автоматизации отрасли'),
(29, 'СК-8', 'Быть способным осуществлять  расчет теплообмена в теплотехнологических процессах и аппаратах'),
(30, 'СК-9', 'Владеть методиками расчета, оценки параметров систем автоматического управления'),
(31, 'СК-10', 'Владеть методиками построения математических моделей'),
(32, 'СК-11', 'Быть способным идентифицировать и определять коэффициенты моделей различных объектов и процессов, а также проводить их оптимизацию.'),
(33, 'СК-12', 'Быть способным применять методики измерения технологических параметров с использованием современных средств и приборов технических измерений '),
(34, 'СК-13', 'Быть способным выбирать технические средства автоматизации для решения различных инженерных задач '),
(35, 'СК-14', 'Быть способным производить расчеты и проектирование отдельных частей и систем автоматизации и управления'),
(36, 'СК-15', 'Быть способным выполнять расчет технологического оборудования отрасли'),
(37, 'СК-16', 'Быть способным применять современные системы автоматизированного проектирования систем управления '),
(38, 'СК-17', 'Быть способным выполнять комплексную автоматизацию технологических процессов отрасли'),
(39, 'СК-18', 'Быть способным выполнять расчет и использовать мехатронные системы для решения инженерных задач'),
(40, 'СК-19', 'Быть способным планировать и производить монтаж, диагностику и поддерживать работоспособность систем автоматизации'),
(41, 'СК-20', 'Быть способным использовать современные интеллектуальные сенсорные устройства для решения инженерных задач'),
(42, 'СК-21', 'Быть способным использовать современные телекоммуникационные технологии для решения инженерных задач'),
(43, 'СК-22', 'Владеть методами, способами и средствами получения, хранения и обработки информации (в том числе и графической)'),
(44, 'СК-23', 'Быть способным применять современные программно-аппаратные системы в системах автоматики'),
(45, 'СК-24', 'Владеть объектно-ориентированным языком программирования и навыками его применения для решения задач в области автоматизации '),
(46, 'СК-25', 'Владеть методами и средствами разработки и использования современных хранилищ данных для решения задач в области автоматизации');

-- --------------------------------------------------------

--
-- Table structure for table `creator_study_programm`
--

CREATE TABLE `creator_study_programm` (
  `id` int(10) NOT NULL,
  `id_teacher` int(10) DEFAULT NULL,
  `id_study_programm` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fakultativ`
--

CREATE TABLE `fakultativ` (
  `id` int(10) NOT NULL,
  `id_plan` int(10) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `hours` int(2) DEFAULT NULL,
  `semester_number` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fakultativ`
--

INSERT INTO `fakultativ` (`id`, `id_plan`, `name`, `hours`, `semester_number`) VALUES
(1, 1, 'английский', 20, '1,2');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(10) NOT NULL,
  `group_name` varchar(10) NOT NULL,
  `count_students` int(10) DEFAULT NULL,
  `id_speciality` int(11) NOT NULL,
  `speciality_form` varchar(20) NOT NULL,
  `year` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `group_name`, `count_students`, `id_speciality`, `speciality_form`, `year`) VALUES
(1, '', 20, 1, '', 0),
(4, '1А-27', NULL, 13, 'дневная', 2013);

-- --------------------------------------------------------

--
-- Table structure for table `group_components`
--

CREATE TABLE `group_components` (
  `id` int(10) NOT NULL,
  `name` enum('Государственный компонент','Компонент учреждения высшего образования','Факультативные дисциплины','Дополнительные виды обучения') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `group_units`
--

CREATE TABLE `group_units` (
  `id` int(10) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `id_group_components` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `node`
--

CREATE TABLE `node` (
  `id` int(10) NOT NULL,
  `id_subject` int(10) DEFAULT NULL,
  `id_cathedra` int(10) DEFAULT NULL,
  `id_plan` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `plan`
--

CREATE TABLE `plan` (
  `id` int(10) NOT NULL,
  `id_group` int(11) DEFAULT NULL,
  `date_approve` date DEFAULT NULL,
  `count_semesters` int(2) DEFAULT NULL,
  `first_year` int(4) DEFAULT NULL,
  `second_year` int(4) DEFAULT NULL,
  `registration_number` varchar(20) DEFAULT NULL,
  `registration_number_standard` varchar(20) DEFAULT NULL,
  `protocol_number` int(10) DEFAULT NULL,
  `date_protocol` date DEFAULT NULL,
  `file_name` varchar(150) DEFAULT NULL,
  `processed` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plan`
--

INSERT INTO `plan` (`id`, `id_group`, `date_approve`, `count_semesters`, `first_year`, `second_year`, `registration_number`, `registration_number_standard`, `protocol_number`, `date_protocol`, `file_name`, `processed`) VALUES
(1, 1, '2013-06-28', 9, 2013, 2014, '22д-1-13/раб2', 'I 53-1-003/тип', 10, '2013-06-28', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `practice`
--

CREATE TABLE `practice` (
  `id` int(10) NOT NULL,
  `id_plan` int(10) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `semestr_number` int(2) DEFAULT NULL,
  `count_weeks` int(2) DEFAULT NULL,
  `ze` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `practice`
--

INSERT INTO `practice` (`id`, `id_plan`, `name`, `semestr_number`, `count_weeks`, `ze`) VALUES
(1, 1, 'технологическая', 7, 3, 2.5);

-- --------------------------------------------------------

--
-- Table structure for table `semestr`
--

CREATE TABLE `semestr` (
  `id` int(10) NOT NULL,
  `number` int(10) NOT NULL,
  `lecture` int(10) DEFAULT NULL,
  `laboratory` int(10) DEFAULT NULL,
  `practice` int(10) DEFAULT NULL,
  `seminar` int(10) DEFAULT NULL,
  `id_type` int(10) DEFAULT NULL,
  `rgr` int(1) DEFAULT NULL,
  `course_work_type` enum('курсовой проект','курсовая работа') DEFAULT NULL,
  `id_node` int(10) DEFAULT NULL,
  `ze` int(10) DEFAULT NULL,
  `cource_work_ze` int(10) DEFAULT NULL,
  `cource_work_hours` int(10) DEFAULT NULL,
  `id_teacher` int(10) DEFAULT NULL,
  `id_faculty` int(10) DEFAULT NULL,
  `diplom_hour` int(10) DEFAULT NULL,
  `diplom_ze` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `speciality`
--

CREATE TABLE `speciality` (
  `id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `shifr` varchar(50) DEFAULT NULL,
  `qualification` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `speciality`
--

INSERT INTO `speciality` (`id`, `name`, `shifr`, `qualification`) VALUES
(1, 'Информационные системы и технологии', '1-40 05 01-01', 'инженер-программист'),
(13, 'Автоматизация технологических процессов и производств (легкая промышленность)', '1-53 01 01-05', 'инженер по автоматизации');

-- --------------------------------------------------------

--
-- Table structure for table `study_programm`
--

CREATE TABLE `study_programm` (
  `id` int(10) NOT NULL,
  `id_subject` int(10) DEFAULT NULL,
  `date_approve` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE `subject` (
  `id` int(10) NOT NULL,
  `name` varchar(200) NOT NULL,
  `shifr` varchar(50) DEFAULT NULL,
  `id_unit` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sub_competence`
--

CREATE TABLE `sub_competence` (
  `id` int(10) NOT NULL,
  `id_competence` int(10) DEFAULT NULL,
  `id_subject` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE `type` (
  `id` int(10) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `koff` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`id`, `name`, `koff`) VALUES
(1, 'зачет', 0.3),
(2, 'диф. зачет', 0.35),
(3, 'экзамен', 0.4),
(4, 'просмотр', 0.4);

-- --------------------------------------------------------

--
-- Table structure for table `weeks_semestr`
--

CREATE TABLE `weeks_semestr` (
  `id` int(10) NOT NULL,
  `number_semestr` int(10) DEFAULT NULL,
  `count_weeks` int(10) DEFAULT NULL,
  `id_semestr` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `certification`
--
ALTER TABLE `certification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKcerf` (`id_plan`);

--
-- Indexes for table `competence`
--
ALTER TABLE `competence`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `creator_study_programm`
--
ALTER TABLE `creator_study_programm`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKcr` (`id_study_programm`);

--
-- Indexes for table `fakultativ`
--
ALTER TABLE `fakultativ`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKfak` (`id_plan`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_speciality` (`id_speciality`);

--
-- Indexes for table `group_components`
--
ALTER TABLE `group_components`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_units`
--
ALTER TABLE `group_units`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKgrunit` (`id_group_components`);

--
-- Indexes for table `node`
--
ALTER TABLE `node`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK` (`id_subject`),
  ADD KEY `FK10` (`id_plan`);

--
-- Indexes for table `plan`
--
ALTER TABLE `plan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_group` (`id_group`);

--
-- Indexes for table `practice`
--
ALTER TABLE `practice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKprac` (`id_plan`);

--
-- Indexes for table `semestr`
--
ALTER TABLE `semestr`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKS` (`id_node`),
  ADD KEY `FKtype` (`id_type`);

--
-- Indexes for table `speciality`
--
ALTER TABLE `speciality`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `study_programm`
--
ALTER TABLE `study_programm`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKstudy` (`id_subject`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKsub` (`id_unit`);

--
-- Indexes for table `sub_competence`
--
ALTER TABLE `sub_competence`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKcom` (`id_competence`),
  ADD KEY `FKcom2` (`id_subject`);

--
-- Indexes for table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `weeks_semestr`
--
ALTER TABLE `weeks_semestr`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKweek` (`id_semestr`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `certification`
--
ALTER TABLE `certification`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `competence`
--
ALTER TABLE `competence`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `creator_study_programm`
--
ALTER TABLE `creator_study_programm`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fakultativ`
--
ALTER TABLE `fakultativ`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `group_components`
--
ALTER TABLE `group_components`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `group_units`
--
ALTER TABLE `group_units`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `node`
--
ALTER TABLE `node`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `plan`
--
ALTER TABLE `plan`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `practice`
--
ALTER TABLE `practice`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `semestr`
--
ALTER TABLE `semestr`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `speciality`
--
ALTER TABLE `speciality`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `study_programm`
--
ALTER TABLE `study_programm`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subject`
--
ALTER TABLE `subject`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=209;
--
-- AUTO_INCREMENT for table `sub_competence`
--
ALTER TABLE `sub_competence`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `type`
--
ALTER TABLE `type`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `weeks_semestr`
--
ALTER TABLE `weeks_semestr`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `certification`
--
ALTER TABLE `certification`
  ADD CONSTRAINT `FKcerf` FOREIGN KEY (`id_plan`) REFERENCES `plan` (`id`);

--
-- Constraints for table `creator_study_programm`
--
ALTER TABLE `creator_study_programm`
  ADD CONSTRAINT `FKcr` FOREIGN KEY (`id_study_programm`) REFERENCES `study_programm` (`id`);

--
-- Constraints for table `fakultativ`
--
ALTER TABLE `fakultativ`
  ADD CONSTRAINT `FKfak` FOREIGN KEY (`id_plan`) REFERENCES `plan` (`id`);

--
-- Constraints for table `groups`
--
ALTER TABLE `groups`
  ADD CONSTRAINT `groups_ibfk_1` FOREIGN KEY (`id_speciality`) REFERENCES `speciality` (`id`);

--
-- Constraints for table `group_units`
--
ALTER TABLE `group_units`
  ADD CONSTRAINT `FKgrunit` FOREIGN KEY (`id_group_components`) REFERENCES `group_components` (`id`);

--
-- Constraints for table `node`
--
ALTER TABLE `node`
  ADD CONSTRAINT `FK` FOREIGN KEY (`id_subject`) REFERENCES `subject` (`id`),
  ADD CONSTRAINT `FK10` FOREIGN KEY (`id_plan`) REFERENCES `plan` (`id`);

--
-- Constraints for table `plan`
--
ALTER TABLE `plan`
  ADD CONSTRAINT `plan_ibfk_1` FOREIGN KEY (`id_group`) REFERENCES `groups` (`id`);

--
-- Constraints for table `practice`
--
ALTER TABLE `practice`
  ADD CONSTRAINT `FKprac` FOREIGN KEY (`id_plan`) REFERENCES `plan` (`id`);

--
-- Constraints for table `semestr`
--
ALTER TABLE `semestr`
  ADD CONSTRAINT `FKS` FOREIGN KEY (`id_node`) REFERENCES `node` (`id`),
  ADD CONSTRAINT `FKtype` FOREIGN KEY (`id_type`) REFERENCES `type` (`id`);

--
-- Constraints for table `study_programm`
--
ALTER TABLE `study_programm`
  ADD CONSTRAINT `FKstudy` FOREIGN KEY (`id_subject`) REFERENCES `subject` (`id`);

--
-- Constraints for table `subject`
--
ALTER TABLE `subject`
  ADD CONSTRAINT `FKsub` FOREIGN KEY (`id_unit`) REFERENCES `group_units` (`id`);

--
-- Constraints for table `sub_competence`
--
ALTER TABLE `sub_competence`
  ADD CONSTRAINT `FKcom` FOREIGN KEY (`id_competence`) REFERENCES `competence` (`id`),
  ADD CONSTRAINT `FKcom2` FOREIGN KEY (`id_subject`) REFERENCES `subject` (`id`);

--
-- Constraints for table `weeks_semestr`
--
ALTER TABLE `weeks_semestr`
  ADD CONSTRAINT `FKweek` FOREIGN KEY (`id_semestr`) REFERENCES `semestr` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
