var express = require('express');
var _ = require('lodash');
var fs = require('fs');

var planModel = require('../models/planModel');
var specialityModel = require('../models/specialityModel');
var groupModel = require('../models/groupModel');
var groupComponents = require('../models/groupComponentsModel');
var groupUnits = require('../models/groupUnitsModel');
var subjectModel = require('../models/subjectModel');
var docxParser = require('../utils/docxParser');
var helpers = require('../utils/helpers');

const FILES_UPLOAD_PATH = `${process.env.UPLOAD_PATH}`;

const { returnError } = helpers;

function mapPlanList(rows) {
  return rows.map(row => {
    let date = new Date(row.date_approve);
    const year = date.getFullYear();
    let month = date.getMonth() + 1;
    month = month < 10 ? "0" + month : month;
    const day = date.getDate();
    const date_approve = [day, month, year].join('.');
    const picked = _.pick(row, ['id', 'shifr', 'year', 'group_name', 'speciality_form', 'registration_number', 'file_name', 'processed']);
    return {
      ...picked,
      date_approve,
    }
  })
}

exports.list = function(req, res, next) {
  planModel.getPlan()
  .then(rows => res.json(mapPlanList(rows)))
  .catch(err => returnError(res, 500, 'Ошибка получения списка планов!', err));
}

exports.download = function(req, res, next) {
  const id = _.get(req, 'params.id');
  if (isNaN(id)) {
    return returnError(res, 500, 'Неверный параметр запроса!');
  }
  planModel.getPlan({
    ['plan.id']: id,
  })
    .then((rows) => {
      if (rows.length === 1) {
        const file = `${FILES_UPLOAD_PATH}${rows[0].file_name}`;
        fs.stat(file, function(err, stat) {
          if(err == null) {
              return res.download(file);
          } else {
              return returnError(res, 404, 'Документ плана не найден!', file);
          }
        });
      } else {
        return returnError(res, 404, 'План не найден!');
      }
    })
    .catch(err => returnError(res, 500, 'Ошибка при поиске плана!', err));
}

const deletePromise = (fileName) => {
  return new Promise((resolve, reject) => {
    fs.unlink(`${FILES_UPLOAD_PATH}${fileName}`, (err) => {
      if (err) reject(err);
      resolve(true);
    });
  });
}

exports.update = function(req, res, next) {
  let id = _.get(req, 'params.id');
  if (isNaN(id)) {
    return returnError(res, 500, 'Неверный параметр запроса!');
  }
  id = parseInt(id);
  if (Object.keys(req.files).length == 0) {
    return returnError(res, 400, 'Нет файлов для загрузки!');
  }

  let document = req.files.file;
  const { data: buffer, name: filename } = document;
  docxParser
    .parseBuffer(buffer)
    .then((regNum) => {
      planModel.getPlan({
        registration_number: regNum,
      })
      .then((rows) => {
        if (rows.length > 0 && rows[0].id != id) {
          return returnError(res, 400, 'План уже существует!');
        }

        planModel.getPlan({
          'plan.id': id,
        })
        .then((rows) => {
          if (rows.length === 0) {
            return returnError(res, 400, `План с id (${id}) не существует!`);
          }

          deletePromise(rows[0].file_name)
            .then(() => {
              document.mv(`${FILES_UPLOAD_PATH}${filename}`, function(err) {
                if (err) {
                  return returnError(res, 500, 'Ошибка сохранения документа!', err);
                }
                planModel.updatePlan({
                  id,
                }, {
                  id_group: null,
                  processed: 0,
                  date_protocol: null,
                  protocol_number: null,
                  registration_number_standard: null,
                  first_year: null,
                  second_year: null,
                  count_semesters: null,
                  date_approve: null,
                  registration_number: regNum,
                  file_name: filename,
                })
                .then(id => res.json({ planId: id }))
                .catch(err => returnError(res, 500, 'Ошибка обновления плана!', err));
              });
            })
            .catch(err => returnError(res, 500, 'Ошибка удаления старого плана!', err))
        })
        .catch(err => returnError(res, 500, 'Ошибка получения списка планов!', err))

      })
      .catch(err => returnError(res, 500, 'Ошибка получения списка планов!', err))
    })
    .catch(err => returnError(res, 500, 'Ошибка обработки документа!', err));
}

// Handle book update on POST.
exports.upload = function(req, res, next) {
  if (Object.keys(req.files).length == 0) {
    return returnError(res, 400, 'Нет файлов для загрузки!');
  }

  let document = req.files.file;
  const { data: buffer, name: filename } = document;
  docxParser
    .parseBuffer(buffer)
    .then((regNum) => {
      planModel.getPlan({
        registration_number: regNum,
      })
      .then((rows) => {
        if (rows.length > 0) {
          return returnError(res, 400, 'План уже существует!');
        }
        document.mv(`${FILES_UPLOAD_PATH}${filename}`, function(err) {
          if (err) {
            return returnError(res, 500, 'Ошибка сохранения документа!', err);
          }
          planModel.insertPlan({
            registration_number: regNum,
            file_name: filename
          })
          .then(id => res.json({ planId: id }))
          .catch(err => returnError(res, 500, 'Ошибка вставки записей!', err));
        });
      })
      .catch(err => returnError(res, 500, 'Ошибка получения записей!', err))
    })
    .catch(err => returnError(res, 500, 'Ошибка обработки документа!', err));
};

function checkSpeciality({ shifr, name, qualification }) {
  return new Promise((resolve, reject) => {
    specialityModel.getSpecialities({
      shifr,
    })
    .then((rows) => {
      if (rows.length === 0) {
        specialityModel.insertSpeciality({ shifr, name, qualification })
        .then((id) => { resolve(id); })
        .catch(err => reject(new Error("Ошибка добавления специальности!")));
      } else {
        resolve(rows[0].id);
      }
    })
    .catch((err) => {
      console.log(err);
      reject(new Error("Ошибка получения списка специальностей!"));
    });
  });
}

function checkGroup({ group_name, year, speciality_form, id_speciality }) {
  return new Promise((resolve, reject) => {
    groupModel.getGroups({
      group_name,
    })
    .then((rows) => {
      if (rows.length === 0) {
        groupModel.insertGroup({ group_name, year, speciality_form, id_speciality })
        .then((id) => { resolve(id); })
        .catch(err => { console.log(err); reject(new Error("Ошибка добавления группы!")); });
      } else {
        resolve(rows[0].id);
      }
    })
    .catch((err) => {
      console.log(err);
      reject(new Error("Ошибка получения списка групп!"))
    });
  });
}

function groupComponentPromise(component) {
  return new Promise((resolve, reject) => {
    const { name } = component;
    groupComponents.getGroupComponents({
      name,
    })
      .then((rows) => {
        if (rows.length === 0) {
          groupComponents.insertGroupComponent({ name })
            .then(id => resolve(id))
            .catch(err => { console.log(err); reject(new Error("Ошибка добавления компонента!")); });
        } else {
          resolve(rows[0].id);
        }
      })
      .catch((err) => {
        console.log(err);
        reject(new Error("Ошибка получения списка компонентов!"))
      });
  })
}

function groupUnitPromise(id_group_components, unit) {
  return new Promise((resolve, reject) => {
    const { name } = unit;
    groupUnits.getGroupUnits({
      name,
      id_group_components,
    })
      .then((rows) => {
        if (rows.length === 0) {
          groupUnits.insertGroupUnit({ name, id_group_components })
            .then(id => resolve(id))
            .catch(err => reject(new Error("Ошибка добавления модуля!")));
        } else {
          resolve(rows[0].id);
        }
      })
      .catch((err) => {
        console.log(err);
        reject(new Error("Ошибка получения списка модулей!"));
      });
  })
}

function subjectPromise(id_unit, subject) {
  return new Promise((resolve, reject) => {
    const { name } = subject;
    subjectModel.getSubjects({
      name,
      id_unit,
    })
      .then((rows) => {
        if (rows.length === 0) {
          subjectModel.insertSubject({ name, id_unit })
            .then(id => resolve(id))
            .catch(err => reject(new Error("Ошибка добавления предмета!")));
        } else {
          resolve(rows[0].id);
        }
      })
      .catch((err) => {
        console.log(err);
        reject(new Error("Ошибка получения списка предметов!"));
      });
  })
}

function checkComponents(component) {
  return new Promise((resolve, reject) => {
    groupComponentPromise(component)
      .then((componentId) => {
        component.items.forEach((unit) => {
          groupUnitPromise(componentId, unit)
            .then((unitId) => {
              if (unit.items.length > 0) {
                unit.items.forEach((subject) => {
                  subjectPromise(unitId, subject)
                    .then(() => {
                      resolve(true);
                    })
                    .catch(err => reject(err));
                })
              } else {
                resolve(true);
              }
            })
            .catch(err => reject(err));
        })
      })
      .catch(err => reject(err));
  });
}

// Handle book update on POST.
exports.process = function(req, res, next) {

  const planId = _.get(req, 'body.planId');

  if (!planId) {
    return returnError(res, 500, 'Неверный запрос!')
  }

  planModel.getPlan({
    ['plan.id']: planId,
  })
  .then((rows) => {
    if (rows.length === 0) {
      return returnError(res, 400, 'План не найден!')
    }

    const { file_name } = rows[0];

    if (!file_name || !fs.existsSync(FILES_UPLOAD_PATH + file_name)) {
      return returnError(res, 400, 'Документ плана не найден!', `Документ плана не найден: ${FILES_UPLOAD_PATH + file_name}`)
    }

    docxParser
      .parseFile(FILES_UPLOAD_PATH + file_name)
      .then((params) => {
        checkSpeciality(params.speciality)
          .then((id_speciality) => {
            checkGroup({ ...params.group, id_speciality })
              .then((id_group) => {
                Promise.all(params.components.map(checkComponents))
                  .then(() => {
                    planModel.updatePlan({
                      id: planId,
                    }, {
                      id_group,
                      date_approve: params.datePlan,
                      first_year: params.years[0],
                      second_year: params.years[1],
                      registration_number_standard: params.planType,
                      protocol_number: params.protocol.number,
                      date_protocol: params.protocol.date,
                      count_semesters: params.semesters,
                      processed: 1,
                    })
                    .then(() => {
                      planModel.getPlan({
                        ['plan.id']: planId,
                      })
                        .then(rows => res.json(mapPlanList(rows)[0]))
                        .catch(err => returnError(res, 500, 'Ошибка получения плана!', err));
                    })
                    .catch(err => returnError(res, 500, 'Ошибка сохранения плана!', err))

                  })
                  .catch(err => returnError(res, 500, err.message, err));
              })
              .catch(err => returnError(res, 500, err.message, err));
          })
          .catch(err => returnError(res, 500, err.message, err));
      })
      .catch(err => returnError(res, 500, 'Ошибка обработки документа!', err));
  })
  .catch(err => returnError(res, 500, 'Ошибка получения документа!', err))

};


exports.delete = function(req, res, next) {
  const id = _.get(req, 'params.id');
  if (isNaN(id)) {
    return returnError(res, 500, 'Неверный параметр запроса!');
  }
  planModel.deletePlan({
    ['plan.id']: id,
  })
    .then(() => {
      res.json({ message: "План удалён!" })
    })
    .catch(err => returnError(res, 500, 'Ошибка при удалении плана!', err));
}
