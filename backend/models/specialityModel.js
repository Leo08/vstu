var knexOptions = require('../knex.config.js');

exports.getSpecialities = function(where) {
  return new Promise(function(resolve, reject) {
    const knex = require('knex')(knexOptions);
    knex
      .from('speciality')
      .select("*")
      .where(where)
      .then(rows => resolve(rows))
      .catch(err => reject(err))
      .finally(() => { knex.destroy(); });
  });
};

exports.insertSpeciality = function(data) {
  return new Promise(function(resolve, reject) {
    const knex = require('knex')(knexOptions);
    knex('speciality')
      .returning('id')
      .insert(data)
      .then(id => resolve(id[0]))
      .catch(err => reject(err))
      .finally(() => { knex.destroy(); });
  });
};

exports.updateSpeciality = function(where, data) {
  return new Promise(function(resolve, reject) {
    const knex = require('knex')(knexOptions);
    knex('speciality')
      .where(where)
      .update(data)
      .then(rows => resolve(rows[0]))
      .catch(err => reject(err))
      .finally(() => { knex.destroy(); });
  });
};
