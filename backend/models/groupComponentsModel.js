var knexOptions = require('../knex.config.js');

exports.getGroupComponents = function(where) {
  return new Promise(function(resolve, reject) {
    const knex = require('knex')(knexOptions);
    knex
      .from('group_components')
      .select("*")
      .where(where)
      .then(rows => resolve(rows))
      .catch(err => reject(err))
      .finally(() => { knex.destroy(); });
  });
};

exports.insertGroupComponent = function(data) {
  return new Promise(function(resolve, reject) {
    const knex = require('knex')(knexOptions);
    knex('group_components')
      .returning('id')
      .insert(data)
      .then(id => resolve(id[0]))
      .catch(err => reject(err))
      .finally(() => { knex.destroy(); });
  });
};

exports.updateGroupComponent = function(where, data) {
  return new Promise(function(resolve, reject) {
    const knex = require('knex')(knexOptions);
    knex('group_components')
      .where(where)
      .update(data)
      .then(rows => resolve(rows[0]))
      .catch(err => reject(err))
      .finally(() => { knex.destroy(); });
  });
};
