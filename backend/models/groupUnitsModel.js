var knexOptions = require('../knex.config.js');

exports.getGroupUnits = function(where) {
  return new Promise(function(resolve, reject) {
    const knex = require('knex')(knexOptions);
    knex
      .from('group_units')
      .select("*")
      .where(where)
      .then(rows => resolve(rows))
      .catch(err => reject(err))
      .finally(() => { knex.destroy(); });
  });
};

exports.insertGroupUnit = function(data) {
  return new Promise(function(resolve, reject) {
    const knex = require('knex')(knexOptions);
    knex('group_units')
      .returning('id')
      .insert(data)
      .then(id => resolve(id[0]))
      .catch(err => reject(err))
      .finally(() => { knex.destroy(); });
  });
};

exports.updateGroupUnit = function(where, data) {
  return new Promise(function(resolve, reject) {
    const knex = require('knex')(knexOptions);
    knex('group_units')
      .where(where)
      .update(data)
      .then(rows => resolve(rows[0]))
      .catch(err => reject(err))
      .finally(() => { knex.destroy(); });
  });
};
