var knexOptions = require('../knex.config.js');

exports.getPlan = function(where = null) {
  return new Promise(function(resolve, reject) {
    const knex = require('knex')(knexOptions);
    const select = [];
    select.push('plan.*');
    select.push('speciality.name');
    select.push('speciality.shifr');
    select.push('speciality.qualification');
    select.push('groups.group_name');
    select.push('groups.count_students');
    select.push('groups.speciality_form');
    select.push('groups.year');
    if (where) {
      knex
        .from('plan')
        .select(select)
        .where(where)
        .leftJoin('groups', 'groups.id', 'plan.id_group')
        .leftJoin('speciality', 'groups.id_speciality', 'speciality.id')
        .then(rows => resolve(rows))
        .catch(err => reject(err))
        .finally(() => { knex.destroy(); });
    } else {
      knex
        .from('plan')
        .select(select)
        .leftJoin('groups', 'groups.id', 'plan.id_group')
        .leftJoin('speciality', 'groups.id_speciality', 'speciality.id')
        .then(rows => resolve(rows))
        .catch(err => reject(err))
        .finally(() => { knex.destroy(); });
    }
  });
};

exports.insertPlan = function(data) {
  return new Promise(function(resolve, reject) {
    const knex = require('knex')(knexOptions);
    knex('plan')
      .returning('id')
      .insert(data)
      .then(id => resolve(id[0]))
      .catch(err => reject(err))
      .finally(() => { knex.destroy(); });
  });
};

exports.updatePlan = function(where, data) {
  return new Promise(function(resolve, reject) {
    const knex = require('knex')(knexOptions);
    knex('plan')
      .where(where)
      .update(data)
      .then(rows => resolve(rows[0]))
      .catch(err => reject(err))
      .finally(() => { knex.destroy(); });
  });
};

exports.deletePlan = function(where) {
  return new Promise(function(resolve, reject) {
    const knex = require('knex')(knexOptions);
    knex('plan')
      .where(where)
      .del()
      .then(() => resolve())
      .catch(err => reject(err))
      .finally(() => { knex.destroy(); });
  });
};