var knexOptions = require('../knex.config.js');

exports.getSubjects = function(where) {
  return new Promise(function(resolve, reject) {
    const knex = require('knex')(knexOptions);
    knex
      .from('subject')
      .select("*")
      .where(where)
      .then(rows => resolve(rows))
      .catch(err => reject(err))
      .finally(() => { knex.destroy(); });
  });
};

exports.insertSubject = function(data) {
  return new Promise(function(resolve, reject) {
    const knex = require('knex')(knexOptions);
    knex('subject')
      .returning('id')
      .insert(data)
      .then(id => resolve(id[0]))
      .catch(err => reject(err))
      .finally(() => { knex.destroy(); });
  });
};

exports.updateSubject = function(where, data) {
  return new Promise(function(resolve, reject) {
    const knex = require('knex')(knexOptions);
    knex('subject')
      .where(where)
      .update(data)
      .then(rows => resolve(rows[0]))
      .catch(err => reject(err))
      .finally(() => { knex.destroy(); });
  });
};
