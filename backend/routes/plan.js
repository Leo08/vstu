var express = require('express');

var planController = require('../controllers/planController');

var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  return res.status(404).json({ message: "Not Found!" });
});

router.get('/list', planController.list);

router.post('/upload', planController.upload);

router.get('/download/:id', planController.download);

router.post('/update/:id', planController.update);

router.post('/process', planController.process);

router.get('/delete/:id', planController.delete);

module.exports = router;
