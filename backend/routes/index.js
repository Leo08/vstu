var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  return res.status(404).json({ message: "Not Found!" });
});

module.exports = router;
