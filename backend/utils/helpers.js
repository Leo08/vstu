exports.returnError = function(res, code = 500, message, err = null) {
  console.log("\x1b[31m%s\x1b[0m" ,"[ERROR]:", err ? err : message);
  return res.status(code).json({ message });
};
