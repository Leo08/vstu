var mammoth = require('mammoth');
var jsdom = require('jsdom');
const { JSDOM } = jsdom;

function getDates(str, multi = false) {
  const dateRegexp = /\d{4}/g;
  const dates = [];
  const newStr = str.replace('г.').trim();
  while ((myArray = dateRegexp.exec(newStr)) != null) {
    dates.push(myArray[0]);
  }
  if (dates.length == 0) {
    return null;
  }
  return multi ? dates : dates[0];
}

function parseDate(date) {
  const monthes = [
    'января',
    'февраля',
    'марта',
    'апреля',
    'мая',
    'июня',
    'июля',
    'августа',
    'сентября',
    'октября',
    'ноября',
    'декабря'
  ];
  const dates = date.split(' ');
  return [dates[2], monthes.indexOf(dates[1].toLowerCase())+1, dates[0]].join('-');
}

exports.parseBuffer = function(buffer) {
  return new Promise(function(resolve, reject) {
    mammoth.convertToHtml({ buffer })
        .then(function(result){
          const html = result.value;
          const dom = new JSDOM(html);
          const regNum = dom.window.document.querySelector('table').querySelector('tr>td>p:last-child>strong').textContent;
          resolve(regNum);
        })
        .catch(err => reject(err))
        .done();
  });
};

function getIndexById(ar, id) {
  for ( let index = 0; index < ar.length; index++ ) {
    if (ar[index].id == id) {
      return index;
    }
  };
  return null;
}

function addToParent(ar, el, id1, id2 = null) {
  const index = getIndexById(ar, id1);
  if (index === null) {
    return ;
  }
  if (id2 === null) {
    return ar[index].items.push(el);
  }
  const index2 = getIndexById(ar[index].items, id2);
  if (index2 === null) {
    return ;
  }
  ar[index].items[index2].items.push(el);
}

exports.parseFile = function(path) {
  return new Promise(function(resolve, reject) {
    mammoth.convertToHtml({ path })
        .then(function(result){

            const html = result.value;
            const dom = new JSDOM(html);
            const{ document } = dom.window;
            const tables = document.querySelectorAll("table");

            const params = Object.create(null);

            const headerCells = tables[0].firstChild.querySelectorAll('td');

            const headerLeftCells = headerCells[0].querySelectorAll('p>strong');
            const headerCenterCells = headerCells[1].querySelectorAll('p>strong');
            const headerRightCells = headerCells[2].querySelectorAll('p>strong');
            
            params.datePlan = parseDate(headerLeftCells[0].textContent);
            params.regNum = headerLeftCells[1].textContent;

            params.speciality = {};
            params.speciality.shifr = /[0-9\- ]*/g.exec(headerCenterCells[2].textContent)[0].trim();
            params.speciality.name = headerCenterCells[2].textContent.replace(params.speciality.shifr, "").trim();
            params.speciality.qualification = headerRightCells[2].textContent.trim();
            
            params.group = {};
            params.group.group_name = headerRightCells[0].textContent;
            params.group.year = getDates(headerRightCells[1].textContent);
            params.group.speciality_form = headerCenterCells[3].textContent.trim();

            params.years = getDates(headerCells[2].querySelector('p').textContent, true);

            const planType = document.querySelectorAll("p>sup");
            params.planType = planType[planType.length - 1].parentNode.querySelector("strong").textContent.trim();

            const protocol = tables[tables.length - 1].querySelectorAll("tr:last-child>td:nth-child(2) p>strong");
            params.protocol = {};
            params.protocol.number = protocol[0].textContent;
            params.protocol.date = protocol[1].textContent.split('.');
            params.protocol.date = [params.protocol.date[2],params.protocol.date[1],params.protocol.date[0]].join('-');

            params.semesters = 0;
            tables[1].querySelectorAll('tr').forEach(row => {
              const text = row.childNodes[0].textContent.trim();
              if (
                (text == 'I') || 
                (text == 'II') || 
                (text == 'III') || 
                (text == 'IV') || 
                (text == 'V') || 
                (text == 'VI')
              ) {
                params.semesters = params.semesters + 2;
              }
            });
            params.semesters--;
            const tableCols = 12 + (params.semesters - 1) * 3;

            params.components = [];

            tables.forEach(table => {
              const rows = table.querySelectorAll('tr');
              rows.forEach(row => {
                if (row.childNodes.length === tableCols) {
                  const cells = row.childNodes;

                  const number = cells[0].textContent.trim();

                  if (number.length > 0) {
                  
                    const numberAr = number.split('.');
                    const title = cells[1].textContent.trim();

                    switch (numberAr.length) {
                      case 2:
                          addToParent(params.components, {
                            id: numberAr[1],
                            name: title,
                            items: [],
                          }, numberAr[0])
                          break;
                      case 3:
                          addToParent(params.components, {
                            id: numberAr[2],
                            name: title,
                          }, numberAr[0], numberAr[1])
                          break;
                      case 1:
                      default:
                          params.components.push({
                            id: numberAr[0],
                            name: title,
                            items: [],
                          });
                          break;
                    }
                  }
                }
              });
            })

            resolve(params);
        })
        .catch(err => reject(err))
        .done();
  });
};
