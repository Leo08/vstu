# vstu
1. Clone the repo
2. Go to `backend/` and `npm i`
3. Go to `frontend/` and `npm i`
4. Import database from `vstu.sql`
5. Change database settings in `backend/.env`
6. Run `npm start` from `backend/` & `frontend/` directories